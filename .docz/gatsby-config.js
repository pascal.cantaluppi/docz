const { mergeWith } = require('docz-utils')
const fs = require('fs-extra')

let custom = {}
const hasGatsbyConfig = fs.existsSync('./gatsby-config.custom.js')

if (hasGatsbyConfig) {
  try {
    custom = require('./gatsby-config.custom')
  } catch (err) {
    console.error(
      `Failed to load your gatsby-config.js file : `,
      JSON.stringify(err),
    )
  }
}

const config = {
  pathPrefix: '/',

  siteMetadata: {
    title: 'Next Gen Documentation',
    description: 'My awesome app using docz',
  },
  plugins: [
    {
      resolve: 'gatsby-theme-docz',
      options: {
        themeConfig: {},
        src: './',
        gatsbyRoot: null,
        themesDir: 'src',
        mdxExtensions: ['.md', '.mdx'],
        docgenConfig: {},
        menu: [],
        mdPlugins: [],
        hastPlugins: [],
        ignore: [],
        typescript: false,
        ts: false,
        propsParser: true,
        'props-parser': true,
        debug: false,
        native: false,
        openBrowser: null,
        o: null,
        open: null,
        'open-browser': null,
        root: 'C:\\Data\\Learning\\docz\\.docz',
        base: '/',
        source: './',
        'gatsby-root': null,
        files: '**/*.{md,markdown,mdx}',
        public: '/public',
        dest: '.docz/dist',
        d: '.docz/dist',
        editBranch: 'master',
        eb: 'master',
        'edit-branch': 'master',
        config: '',
        title: 'Next Gen Documentation',
        description: 'My awesome app using docz',
        host: 'localhost',
        port: 3000,
        p: 3000,
        separator: '-',
        paths: {
          root: 'C:\\Data\\Learning\\docz',
          templates:
            'C:\\Data\\Learning\\docz\\node_modules\\docz-core\\dist\\templates',
          docz: 'C:\\Data\\Learning\\docz\\.docz',
          cache: 'C:\\Data\\Learning\\docz\\.docz\\.cache',
          app: 'C:\\Data\\Learning\\docz\\.docz\\app',
          appPackageJson: 'C:\\Data\\Learning\\docz\\package.json',
          appTsConfig: 'C:\\Data\\Learning\\docz\\tsconfig.json',
          gatsbyConfig: 'C:\\Data\\Learning\\docz\\gatsby-config.js',
          gatsbyBrowser: 'C:\\Data\\Learning\\docz\\gatsby-browser.js',
          gatsbyNode: 'C:\\Data\\Learning\\docz\\gatsby-node.js',
          gatsbySSR: 'C:\\Data\\Learning\\docz\\gatsby-ssr.js',
          importsJs: 'C:\\Data\\Learning\\docz\\.docz\\app\\imports.js',
          rootJs: 'C:\\Data\\Learning\\docz\\.docz\\app\\root.jsx',
          indexJs: 'C:\\Data\\Learning\\docz\\.docz\\app\\index.jsx',
          indexHtml: 'C:\\Data\\Learning\\docz\\.docz\\app\\index.html',
          db: 'C:\\Data\\Learning\\docz\\.docz\\app\\db.json',
        },
      },
    },
  ],
}

const merge = mergeWith((objValue, srcValue) => {
  if (Array.isArray(objValue)) {
    return objValue.concat(srcValue)
  }
})

module.exports = merge(config, custom)
