const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-dev-404-page-js": hot(preferDefault(require("C:\\Data\\Learning\\docz\\.docz\\.cache\\dev-404-page.js"))),
  "component---readme-md": hot(preferDefault(require("C:\\Data\\Learning\\docz\\README.md"))),
  "component---src-hello-mdx": hot(preferDefault(require("C:\\Data\\Learning\\docz\\src\\hello.mdx"))),
  "component---src-pages-404-js": hot(preferDefault(require("C:\\Data\\Learning\\docz\\.docz\\src\\pages\\404.js")))
}

